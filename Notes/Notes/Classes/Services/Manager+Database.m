//
//  Manager+Database.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager+Database.h"
#define kDBname @"notesDB.sqlite"
#import <sqlite3.h>

@implementation Manager (Database)

-(void)checkIfDatabaseExists {
    
    // Check if the database file exists in the documents directory.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    self.databasePath = [documentsDirectory stringByAppendingPathComponent:kDBname];
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.databasePath]) {
        sqlite3 *sqliteDB;
        int openDatabase = sqlite3_open_v2([self.databasePath UTF8String], &sqliteDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
        if(openDatabase == SQLITE_OK) {
            sqlite3_stmt *statement;
            NSString *query = @"CREATE TABLE notes(noteId integer primary key, noteText text, lastUpdateDate integer)";
            const char *query_stmt = [query UTF8String];
            if(sqlite3_prepare_v2(sqliteDB, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
                if (sqlite3_step(statement) == SQLITE_DONE) {
                    NSLog(@"Database succesfully created");
                } else {
                    NSLog(@"Error creating Database");
                }
            } else {
                NSLog(@"Query is not executed");
            }
            sqlite3_finalize(statement);
        } else {
            NSLog(@"Database is not properly opened!");
        }
        sqlite3_close(sqliteDB);
    }
}


#pragma mark - Communications with DB

-(void)loadAllNotesFromDatabase {
    
    sqlite3 *sqliteDB;
    int openDatabase = sqlite3_open([self.databasePath UTF8String], &sqliteDB);
    if (openDatabase == SQLITE_OK) {
        sqlite3_stmt *statement;
        NSString *selectAllQuery = @"SELECT * FROM notes ORDER BY lastUpdateDate DESC";
        const char *query_stmt = [selectAllQuery UTF8String];
        if (sqlite3_prepare_v2(sqliteDB, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            [self.notesArray removeAllObjects];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int noteId = sqlite3_column_int(statement, 0);
                char *noteChar = (char *) sqlite3_column_text(statement, 1);
                int updateDate = sqlite3_column_int(statement, 2);
                NSString *noteText = [NSString stringWithUTF8String:noteChar];
                NoteModel *note = [[NoteModel alloc] initWithNoteId:noteId noteContent:noteText date:updateDate];
                [self.notesArray addObject:note];
            }
        } else {
            NSLog(@"Query is not executed!");
        }
        sqlite3_finalize(statement);
    } else {
        NSLog(@"Database is not properly opened!");
    }
    
    sqlite3_close(sqliteDB);
    
    
}

-(long)insertNoteIntoDatabaseWithText:(NSString *)noteText andDate:(int)date {
    
    long newNoteId = -1;
    sqlite3 *sqliteDB;
    int openDatabase = sqlite3_open([self.databasePath UTF8String], &sqliteDB);
    if (openDatabase == SQLITE_OK) {
        
        sqlite3_stmt *statement;
        NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO notes ('noteText', lastUpdateDate) VALUES('%@', %d)", noteText, date];
        const char *insert_stmt = [insertQuery UTF8String];
        if(sqlite3_prepare_v2(sqliteDB, insert_stmt, -1, &statement, NULL) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_DONE) {
                
                newNoteId = sqlite3_last_insert_rowid(sqliteDB);
                NoteModel *note = [[NoteModel alloc] initWithNoteId:(int)newNoteId noteContent:noteText date:date];
                [self.notesArray insertObject:note atIndex:0];
                
            } else {
                NSLog(@"Query is not executed!");
            }
        } else {
            NSLog(@"prepare failed");
        }
        
        sqlite3_finalize(statement);
    } else {
        NSLog(@"Database is not properly opened!");
    }
    
    sqlite3_close(sqliteDB);
    return  newNoteId;
    
}

-(BOOL)updateNoteWithId:(int)noteId withNewText:(NSString *)noteText andNewDate:(int)date {
    BOOL success = NO;
    sqlite3 *sqliteDB;
    int openDatabase = sqlite3_open([self.databasePath UTF8String], &sqliteDB);
    if (openDatabase == SQLITE_OK) {
        sqlite3_stmt *statement;
        NSString *updateQuery = [NSString stringWithFormat:@"UPDATE notes SET noteText='%@', lastUpdateDate=%d where noteId=%d", noteText, date, noteId];
        const char *insert_stmt = [updateQuery UTF8String];
        if(sqlite3_prepare_v2(sqliteDB, insert_stmt, -1, &statement, NULL ) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_DONE) {
                if(sqlite3_changes(sqliteDB) == 1) {
                    success = YES;
                } else {
                    NSLog(@"Changes not applied");
                }
            } else {
                NSLog(@"Query is not executed!");
            }
        } else {
            NSLog(@"prepare failed");
        }
        sqlite3_finalize(statement);
    } else {
        NSLog(@"Database is not properly opened!");
    }
    
    sqlite3_close(sqliteDB);
    return  success;
    
}

- (BOOL)deleteNoteWithIdFromDatabase:(int)noteId {
    BOOL success = NO;
    sqlite3 *sqliteDB;
    int openDatabase = sqlite3_open([self.databasePath UTF8String], &sqliteDB);

    if (openDatabase == SQLITE_OK) {
        if (noteId > 0) {
            sqlite3_stmt *statement;
            NSString *deleteQuery = [NSString stringWithFormat:@"DELETE FROM notes WHERE noteId = ?"];
            const char *delete_stmt = [deleteQuery UTF8String];
            
            if(sqlite3_prepare_v2(sqliteDB, delete_stmt, -1, &statement, NULL ) == SQLITE_OK) {
                sqlite3_bind_int(statement, 1, noteId);
                if (sqlite3_step(statement) == SQLITE_DONE) {
                    success = YES;
                } else {
                    NSLog(@"delete failed");
                }
            } else {
                NSLog(@"prepare failed");
            }
            sqlite3_finalize(statement);
        }
        
        
    } else {
        NSLog(@"Database is not properly opened!");
    }
    
    sqlite3_close(sqliteDB);
    
    return success;
}


@end
