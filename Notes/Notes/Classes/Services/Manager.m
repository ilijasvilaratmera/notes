//
//  Manager.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager.h"


@implementation Manager

+(Manager *)sharedInstance {
    static Manager *sharedObject = nil;
    static dispatch_once_t _singletonPredicate;
    
    dispatch_once(&_singletonPredicate, ^{
        sharedObject = [[Manager alloc] init];
    });
    
    return sharedObject;
}

-(id)init {
    self = [super init];
    if(self != nil) {
        _notesArray = [[NSMutableArray alloc] init];
        _userProfile = [[UserProfile alloc] init];
        [_userProfile load];
    }
    return self;
}


@end
