//
//  Manager+Database.h
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "Manager.h"
#import "NoteModel.h"

@interface Manager (Database)

-(void)checkIfDatabaseExists;
-(void)loadAllNotesFromDatabase;
-(long)insertNoteIntoDatabaseWithText:(NSString *)noteText andDate:(int)date;
-(BOOL)updateNoteWithId:(int)noteId withNewText:(NSString *)noteText andNewDate:(int)date;
-(BOOL)deleteNoteWithIdFromDatabase:(int)noteId;

@end
