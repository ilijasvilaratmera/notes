//
//  Manager.h
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"

@interface Manager : NSObject

@property (nonatomic, strong) UserProfile *userProfile;
@property (nonatomic, strong, readonly) NSMutableArray *notesArray;
@property (nonatomic, copy) NSString *databasePath;

+(Manager *)sharedInstance;


@end
