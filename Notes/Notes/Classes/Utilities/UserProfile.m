//
//  UserProfile.m
//  Notes
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile

-(instancetype)init {
    
    self= [super init];
    if(self != nil) {
        NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:[UIColor orangeColor]];
        _selectedColor = colorData;
    }
    
    return self;
    
}

-(void)save {
    
    NSMutableDictionary *upDictionary = [[NSMutableDictionary alloc] init];
    [upDictionary setObject:self.selectedColor forKey:@"SelectedHighlightColor"];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:upDictionary forKey:@"UserDefaults"];
    [ud synchronize];

}

-(void)load {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSDictionary *upDictionary = [ud dictionaryForKey:@"UserDefaults"];
    if (upDictionary != nil) {
        if ([upDictionary objectForKey:@"SelectedHighlightColor"] != nil) {
            self.selectedColor = [upDictionary objectForKey:@"SelectedHighlightColor"];
        }
    } else {
        [self save];
    }
    
}


@end
