//
//  UserProfile.h
//  Notes
//
//  Created by Ilija Svilar on 7/6/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface UserProfile : NSObject

@property (strong, nonatomic) NSData *selectedColor;

-(void)save;
-(void)load;

@end
