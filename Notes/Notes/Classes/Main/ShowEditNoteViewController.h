//
//  ShowEditNoteViewController.h
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoteEditedDelegate <NSObject>
@optional
-(void)noteEdited;

@end

@interface ShowEditNoteViewController : UIViewController

@property (weak, nonatomic) id<NoteEditedDelegate>noteEditedDelegate;
@property (assign, nonatomic) NSInteger selectedIndex;

@end
