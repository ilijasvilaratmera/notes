//
//  ShowEditNoteViewController.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "ShowEditNoteViewController.h"
#import "Manager.h"
#import "Manager+Database.h"
#import "NoteModel.h"
#import "MainViewController.h"

@interface ShowEditNoteViewController () <UITextViewDelegate,UINavigationBarDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) NoteModel *selectedNote;
@end

@implementation ShowEditNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveDataToDatabase) name:UIApplicationWillResignActiveNotification object:nil];
    self.navigationController.delegate = self;
    self.selectedNote = [Manager sharedInstance].notesArray[self.selectedIndex];
    self.textView.text = self.selectedNote.noteText;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if([viewController isKindOfClass:[MainViewController class]]) {
        [self saveDataToDatabase];
    }
}

-(void)saveDataToDatabase{
    
    if(self.textView.text.length >0) {
        if(![self.textView.text isEqualToString:self.selectedNote.noteText]) {
            NSDate *noteDate = [NSDate date];
            int timestamp = [noteDate timeIntervalSince1970];
            [[Manager sharedInstance] updateNoteWithId:self.selectedNote.noteId withNewText:self.textView.text andNewDate:timestamp];
            if(self.noteEditedDelegate != nil) {
                [self.noteEditedDelegate noteEdited];
            }
        }
    } else {
        [[Manager sharedInstance] deleteNoteWithIdFromDatabase:self.selectedNote.noteId];
        if(self.noteEditedDelegate != nil) {
            [self.noteEditedDelegate noteEdited];
        }
    }
    
}

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
