//
//  NewNoteViewController.h
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NoteAddedDelegate <NSObject>
@optional

-(void)noteAdded;

@end

@interface NewNoteViewController : UIViewController

@property (nonatomic, weak) id<NoteAddedDelegate>noteAddedDelegate;
@end
