//
//  MainViewController.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "MainViewController.h"
#import "MainTableViewCell.h"
#import "Manager.h"
#import "Manager+Database.h"
#import "UserProfile.h"
#import "NoteModel.h"
#import "ShowEditNoteViewController.h"
#import "NewNoteViewController.h"

@interface MainViewController () <UITableViewDelegate,UITableViewDataSource, UISearchResultsUpdating, NoteAddedDelegate, NoteEditedDelegate>

@property (strong, nonatomic) NSMutableArray *notesArray;
@property (strong, nonatomic) NSMutableArray *filteredNotesArray;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *createNoteButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsButton;
@property (weak, nonatomic) IBOutlet UILabel *noteCountLabel;
@property (strong, nonatomic) UISearchController *searchController;
@property BOOL isSearchActive;
@property (copy,nonatomic) NSString *searchWord;
@property (strong, nonatomic) NSDateFormatter *dateFormater;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchWord = @"";
    self.isSearchActive = NO;
     self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
    self.mainTableView.tableHeaderView = self.searchController.searchBar;
    self.mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self setupDateFormatter];    
    [self loadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
    self.noteCountLabel.text = [NSString stringWithFormat:@"%lu notes",(unsigned long)[Manager sharedInstance].notesArray.count];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
    [self.mainTableView setContentOffset:CGPointMake(0, self.searchController.searchBar.frame.size.height)];

}

-(void)setupDateFormatter {
    
    self.dateFormater = [[NSDateFormatter alloc] init];
    [self.dateFormater setDateFormat:@"dd/MM/yy"];

}

-(void)loadData {
    
    [[Manager sharedInstance] loadAllNotesFromDatabase];
    [self.mainTableView reloadData];
    self.noteCountLabel.text = [NSString stringWithFormat:@"%lu notes",(unsigned long)[Manager sharedInstance].notesArray.count];

}

#pragma mark TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isSearchActive == YES) {
        return self.filteredNotesArray.count;
    } else {
        return [Manager sharedInstance].notesArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MainTableViewCell *cell = (MainTableViewCell *)[self.mainTableView dequeueReusableCellWithIdentifier:@"mainTableCellIdentifier" forIndexPath:indexPath];
    NoteModel *note = nil;
    if(self.isSearchActive == YES) {
        note = self.filteredNotesArray[indexPath.row];
        cell.noteLabel.text = note.noteText;
        [cell.noteLabel setAttributedText:[self highlightSearchWordInNoteText:note.noteText]];
    } else {
        note = [Manager sharedInstance].notesArray[indexPath.row];
        cell.noteLabel.text = note.noteText;
    }
    
    NSString *dateString = [self formatDateStringWithTimestamp:note.lastUpdateDate];
    cell.dateLabel.text = dateString;
    
    return (MainTableViewCell *)cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"fromMainToShowEditNoteSegue" sender:nil];
    
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

#pragma mark Format Date and Highlight search methods

-(NSString *)formatDateStringWithTimestamp:(int)timestamp {
    
    NSDate *noteDate = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSString *formatedStringDate = [self.dateFormater stringFromDate:noteDate];
    
    return formatedStringDate;
}

-(NSMutableAttributedString *)highlightSearchWordInNoteText:(NSString *)noteText {
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc]initWithString:noteText];
    UserProfile *up = [Manager sharedInstance].userProfile;
    UIColor *highlightColor  = [NSKeyedUnarchiver unarchiveObjectWithData:up.selectedColor];
    NSUInteger length = [attrString length];
    NSRange range = NSMakeRange(0, length);
    NSString *word = [self.searchWord localizedLowercaseString];
    NSString *lowercaseString = [[attrString string] localizedLowercaseString];
    while(range.location != NSNotFound)
    {
        range = [lowercaseString rangeOfString:word options:0 range:range];
        if(range.location != NSNotFound) {
            [attrString addAttribute:NSForegroundColorAttributeName value:highlightColor range:range];
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
            
        }
    }
    
    return attrString;
    
}

#pragma mark Search Methods

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    [self searchThroughNotesWithText:self.searchController.searchBar.text];
}


-(void)searchThroughNotesWithText:(NSString *)searchText {
    
    NSMutableArray *a = [[NSMutableArray alloc] init];
    if(![searchText isEqualToString:@""] && ![searchText isEqualToString:@" "]) {
        NSArray *splitedStringArray = [searchText componentsSeparatedByString:@" "];
        for (NoteModel *note in [Manager sharedInstance].notesArray) {
            @autoreleasepool {
                for (NSString *str in splitedStringArray) {
                    if([[note.noteText localizedLowercaseString] containsString:[str localizedLowercaseString]]) {
                        if (![a containsObject:note]) {
                            [a addObject:note];
                        }
                    }
                }
            }
        }
    }
    
    if(a.count >0) {
        self.searchWord = searchText;
        self.filteredNotesArray = a;
        self.isSearchActive = YES;
        [self.mainTableView reloadData];
    } else {
        self.searchWord = @"";
        self.isSearchActive = NO;
        [self.mainTableView reloadData];
    }
    
}

#pragma mark Delegate methods

-(void)noteEdited {
    
    [self loadData];
}

-(void)noteAdded {
    
    [self loadData];
}

- (IBAction)createNoteButtonPressed:(UIBarButtonItem *)sender {
   
    [self performSegueWithIdentifier:@"fromMainToNewNoteSegue" sender:nil];

}

- (IBAction)settingButtonPressed:(UIBarButtonItem *)sender {
    
    [self performSegueWithIdentifier:@"fromMainToSettingsSegue" sender:nil];
}



#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"fromMainToShowEditNoteSegue"]) {
        ShowEditNoteViewController *vc = (ShowEditNoteViewController *)[segue destinationViewController];
        NSIndexPath *index = [self.mainTableView indexPathForSelectedRow];
        if(vc) {
            vc.selectedIndex = index.row;
            vc.noteEditedDelegate = self;
        }
        [self.mainTableView deselectRowAtIndexPath:index animated:NO];
        
    } else if ([segue.identifier isEqualToString:@"fromMainToNewNoteSegue"]) {
        NewNoteViewController *vc = (NewNoteViewController *)[segue destinationViewController];
        if(vc) {
            vc.noteAddedDelegate = self;
        }
    }
    
}


@end
