//
//  SettingsViewController.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "SettingsViewController.h"
#import "Manager.h"
#import "UserProfile.h"

@interface SettingsViewController () <UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIPickerView *colorPicker;
@property (strong, nonatomic) NSArray *colorArray;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.colorArray = [[NSArray alloc]initWithObjects:[UIColor redColor],[UIColor orangeColor],[UIColor yellowColor], [UIColor greenColor], [UIColor cyanColor], [UIColor blueColor],[UIColor magentaColor],[UIColor purpleColor],[UIColor brownColor], nil];
   
    self.colorPicker.delegate = self;
    self.colorPicker.dataSource = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UserProfile *up = [Manager sharedInstance].userProfile;
    UIColor *highlightColor  = [NSKeyedUnarchiver unarchiveObjectWithData:up.selectedColor];
    NSUInteger index = [self.colorArray indexOfObject:highlightColor];
    [self.colorPicker selectRow:index inComponent:0 animated:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonPressed:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)saveButtonPressed:(UIBarButtonItem *)sender {
    
    UserProfile *up = [Manager sharedInstance].userProfile;
    NSUInteger index = [self.colorPicker selectedRowInComponent:0];
    UIColor *selectedcolor = self.colorArray[index];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:selectedcolor];
    up.selectedColor = colorData;
    [up save];
    [self dismissViewControllerAnimated:NO completion:nil];

}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.colorArray.count;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *pickerLabel = [[UILabel alloc] initWithFrame:view.frame];
    pickerLabel.backgroundColor = self.colorArray[row];
    
    return pickerLabel;
}

@end
