//
//  MainViewController.h
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController 
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end
