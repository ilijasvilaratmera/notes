//
//  NewNoteViewController.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "NewNoteViewController.h"
#import "Manager+Database.h"
#import "NoteModel.h"
#import "MainViewController.h"

@interface NewNoteViewController () <UITextViewDelegate,UINavigationBarDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textField;

@end

@implementation NewNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveDataToDatabase) name:UIApplicationWillResignActiveNotification object:nil];
    self.navigationController.delegate = self;
    [self.textField becomeFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {

    if([viewController isKindOfClass:[MainViewController class]]) {
        [self saveDataToDatabase];
    }
}

-(void)saveDataToDatabase{
    
    if(self.textField.text.length >0) {
        NSDate *noteDate = [NSDate date];
        int timestamp = [noteDate timeIntervalSince1970];
        [[Manager sharedInstance] insertNoteIntoDatabaseWithText:self.textField.text andDate:timestamp];
        if(self.noteAddedDelegate != nil) {
            [self.noteAddedDelegate noteAdded];
        }
    }
    
}

-(void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
}

@end
