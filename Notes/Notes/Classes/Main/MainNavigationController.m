//
//  MainNavigationController.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "MainNavigationController.h"

@implementation MainNavigationController

-(void)viewDidLoad {
    
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor colorWithRed:255.0/255.0f green:143.0/255.0f blue:57.0/255.0f alpha:1.0f]};
    self.navigationBar.tintColor = [UIColor colorWithRed:255.0/255.0f green:143.0/255.0f blue:57.0/255.0f alpha:1.0f];
    
}




@end
