//
//  NoteModel.h
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteModel : NSObject

@property (assign, nonatomic) int noteId;
@property (copy, nonatomic) NSString *noteText;
@property (assign, nonatomic) int lastUpdateDate;

-(instancetype)initWithNoteId:(int)noteId noteContent:(NSString *)noteContent date:(int)date;

@end
