//
//  NoteModel.m
//  Notes
//
//  Created by Ilija Svilar on 6/21/16.
//  Copyright © 2016 Mera Software Services. All rights reserved.
//

#import "NoteModel.h"

@implementation NoteModel

-(instancetype)initWithNoteId:(int)noteId noteContent:(NSString *)noteContent date:(int)date {
    
    self = [super init];
    if(self != nil) {
        _noteId = noteId;
        _noteText = noteContent;
        _lastUpdateDate = date;
        
    }
    
    return self;
    
}


@end
